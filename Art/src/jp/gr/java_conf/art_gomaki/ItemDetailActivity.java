package jp.gr.java_conf.art_gomaki;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class ItemDetailActivity extends Activity implements OnClickListener {
	private TextView mTitle;
	private TextView mDescr;
	private TextView mLink;
	private Button btn1, btn2, btn3, btn4, btn5;
	private String link = "";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.item_detail);

		Intent intent = getIntent();

		String title = intent.getStringExtra("TITLE");
		mTitle = (TextView) findViewById(R.id.textView1);
		mTitle.setText(title);
		String descr = intent.getStringExtra("DESCRIPTION");
		mDescr = (TextView) findViewById(R.id.textView2);
		mDescr.setText(descr);
		String pdate = intent.getStringExtra("PDATE");
		mDescr = (TextView) findViewById(R.id.textView4);
		mDescr.setText(pdate);
		link = intent.getStringExtra("LINK");
//		mLink = (TextView) findViewById(R.id.textView4);
//		mLink.setText(link);

		btn1 = (Button)findViewById(R.id.button1);
        btn1.setOnClickListener(this);
//		btn2 = (Button)findViewById(R.id.button2);
//        btn2.setOnClickListener(this);
		btn3 = (Button)findViewById(R.id.button3);
        btn3.setOnClickListener(this);
		btn4 = (Button)findViewById(R.id.button4);
        btn4.setOnClickListener(this);

        // もっと見るボタン
		btn5 = (Button)findViewById(R.id.button_more);
        btn5.setOnClickListener(this);
        
	}

	public void onClick(View v) {
		if(v == btn1) {
			Intent intent = new Intent(this, MainActivity.class);
			startActivity(intent);
		}
		if(v == btn2) {
			Intent intent = new Intent(this, ImageListActivity.class);
			startActivity(intent);
		}
		else if(v == btn3) {
			Intent intent = new Intent(this, FriendsSearchActivity.class);
			startActivity(intent);
		}
		else if(v == btn4) {
			Uri uri = Uri.parse("http://ameblo.jp/gotomaki-923/");
			Intent intent = new Intent(Intent.ACTION_VIEW,uri);
			startActivity(intent);
		}
		else if(v == btn5) {
			Uri uri = Uri.parse(link);
			Intent intent = new Intent(Intent.ACTION_VIEW,uri);
			startActivity(intent);
		}
	}

}