package jp.gr.java_conf.art_gomaki;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

public class ExpandImageViewActivity extends Activity {
	private ImageView imgStr;
	private Button btn1, btn2, btn3, btn4;

    // references to our images
    private Integer[] mThumbIds = {
            R.drawable.sample001,
            R.drawable.sample002,
            R.drawable.sample003,
            R.drawable.sample004,
            R.drawable.sample005,
            R.drawable.sample006,
            R.drawable.sample007,
            R.drawable.sample008,
            R.drawable.sample009,
            R.drawable.sample010,
            R.drawable.sample011,
            R.drawable.sample012,
            R.drawable.sample013,
            R.drawable.sample014,
            R.drawable.sample015,
            R.drawable.sample016,
            R.drawable.sample017,
            R.drawable.sample018,
 //           R.drawable.sample019,
 //           R.drawable.sample020
    };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        Bitmap myImage = null;

		// タイトルバーを非表示にする（setContentViewの前に呼ぶ）
		//requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.expand_image_view);

		Intent intent = getIntent();

		//Bundle b = intent.getExtras();
		///Bitmap bmp = (Bitmap) b.get("data");
		String title = intent.getStringExtra("id");
		int id = Integer.parseInt(title);

        Resources resM = getResources();

        ImageView imageView = (ImageView)findViewById(R.id.gridView1);
        myImage = BitmapFactory.decodeResource(resM, mThumbIds[id]);
        imageView.setImageDrawable(null);
        imageView.setImageBitmap(null);
        imageView.setImageBitmap(myImage);

		imageView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// 拡大した画像をタップするともとの画面に戻る
				Intent intent = new Intent();
				setResult(RESULT_OK, intent);
				finish();
			}
		});
	}


}