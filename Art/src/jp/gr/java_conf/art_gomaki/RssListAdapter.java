package jp.gr.java_conf.art_gomaki;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class RssListAdapter extends ArrayAdapter<Item> {

	private LayoutInflater mInflater;
	private TextView mTitle;
	private TextView mDescr;
	private TextView mPDate;

	public RssListAdapter(Context context, List<Item> objects) {
		super(context, 0, objects);
		// TODO Auto-generated constructor stub
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	// 一行ごとのビューを作成する。
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (convertView == null) {
			view = mInflater.inflate(R.layout.item_row, null);
		}
		// 何番目の値を取得？
		Item item = this.getItem(position);
		if (item != null) {
			String title = item.getTitle().toString();
			mTitle = (TextView) view.findViewById(R.id.textView1);
			mTitle.setText(title);

			String pdate = item.getPDate().toString();
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss", Locale.ENGLISH);
				sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
				Date gmtDate = sdf.parse(pdate);

				sdf = new SimpleDateFormat("yyyy年MM月dd日 kk:mm");
				sdf.setTimeZone(TimeZone.getTimeZone("JST"));
				pdate = sdf.format(gmtDate).toString();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			item.setPDate(pdate);
			mPDate = (TextView) view.findViewById(R.id.textView3);
			mPDate.setText(pdate);
			//String descr = item.getDescription().toString();
			//mDescr = (TextView) view.findViewById(R.id.textView2);
			//mDescr.setText(descr);
		}
		return view;
	}
}