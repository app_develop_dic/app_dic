package jp.gr.java_conf.art_gomaki;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;

//RssParserTask.java
public class GetPhoneBook extends AsyncTask<String, Integer, PhoneBookAdapter> {

	private FriendsSearchActivity fsActivity;
	private PhoneBookAdapter pbAdapter;
	private ProgressDialog fsProgressDialog;
	private ListView fsListView;
	
	// コンストラクタ
	public GetPhoneBook(FriendsSearchActivity activity, PhoneBookAdapter adapter, ListView _listView) {
		fsActivity = activity;
		fsListView = _listView;
		pbAdapter = adapter;
	}

	// タスクを実行した直後にコールされる
	@Override
	protected void onPreExecute() {
		// プログレスバーを表示する
		fsProgressDialog = new ProgressDialog(fsActivity);
		fsProgressDialog.setMessage("Now Loading...");
		fsProgressDialog.show();
	}

	// バックグラウンドにおける処理を担う。タスク実行時に渡された値を引数とする
	@Override
	protected PhoneBookAdapter doInBackground(String... params) {
		
		PhoneBookAdapter result = null;
    	String DB_Path = params[0];	// メインスレッドからDB名を取得する
		
		try {
			result = getPhoneBookDatas(DB_Path);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	// メインスレッド上で実行される
	protected void onPostExecute(PhoneBookAdapter result) {
		fsProgressDialog.dismiss();
		fsListView.setAdapter(result);
	}

	public PhoneBookAdapter getPhoneBookDatas(String DB_Path) throws IOException {
		
    	String name = "";
		int status = 0;	

		try {
			SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(DB_Path,  null);

	        //レコード検索用のクエリ文
			String query_select = "SELECT * FROM same_app_userd_users";

			Cursor cursor = db.rawQuery(query_select, null);
			FriendsSearch fs = null;
			while(cursor.moveToNext()) {
				name = cursor.getString(cursor.getColumnIndex("name"));
				status = cursor.getInt(cursor.getColumnIndex("used_status"));

				if(!name.equals("") && status == 1) {
					fs = new FriendsSearch();
					fs.setName(name);
					fs.setAppInstall(status);
					
					pbAdapter.add(fs);
				}
			}	        
		} catch (SQLException e) {
	        	Log.e("ERROR", e.toString());
	    }
		
		return pbAdapter;
	}

	private String IntegerParthInt(ByteArrayOutputStream outputStream) {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}
}