package jp.gr.java_conf.art_gomaki;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

@SuppressLint("NewApi")
public class ImageListActivity extends Activity implements OnClickListener {
	
    // どこで呼び出したのかを判別するためのコード
 	private static final int RESULTCODE = 1;
 	
 	private AQuery mAq;
 	private ArrayList<HashMap<String, String>> imageData;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_image_list);
        
        mAq = new AQuery(this);
        mAq.id(R.id.button1).clicked(this);
        mAq.id(R.id.button2).clicked(this);
        mAq.id(R.id.button3).clicked(this);
        mAq.id(R.id.button4).clicked(this);
        
        this.getImageList();
	}
	
	// 画像情報の一覧を取得
	private void getImageList(){
		imageData = new ArrayList<HashMap<String,String>>();
		String url = Const.apiDomain+"/app/imageinfo.php?app_id="+Const.appId;
		ProgressDialog progress = new ProgressDialog(this);
	    progress.setTitle("通信中");
	    progress.setMessage("一覧を取得しています。");
		mAq.progress(progress).ajax(url, JSONObject.class, new AjaxCallback<JSONObject>() {
	        @Override
	        public void callback(String url, JSONObject json, AjaxStatus status) {
	            if (json != null) {
	                Log.i("TEST", "JSON: " + json.toString());
	                // パース
	                try {
						JSONArray ary = json.getJSONArray("data");
						for (int i=0; i<ary.length(); i++) {
							HashMap<String, String> mp = new HashMap<String, String>();
							mp.put("title",	ary.getJSONObject(i).getString("title"));
							mp.put("img",	ary.getJSONObject(i).getString("img"));
							mp.put("thum",	ary.getJSONObject(i).getString("thum"));
							imageData.add(mp);
						}
					} catch (JSONException e) {
						// TODO 自動生成された catch ブロック
						e.printStackTrace();
						return;
					}
	                // データ表示
	                setGridAdapter();
	            } else {
	                Toast.makeText(mAq.getContext(), "Error:" + status.getCode(), Toast.LENGTH_LONG)
	                        .show();
	            }
	        }
	    });
	}

	public void onClick(View v) {
		int id = v.getId();
		Intent intent = null;
		switch (id) {
			case R.id.button1:
				intent = new Intent(this, MainActivity.class);
				startActivity(intent);
				break;
			case R.id.button2:
				if (this.getClass() == ImageListActivity.class) {
					return;
				}
				intent = new Intent(this, ImageListActivity.class);
				startActivity(intent);
				break;
			case R.id.button3:
				intent = new Intent(this, FriendsSearchActivity.class);
				startActivity(intent);
				break;
			case R.id.button4:
				Uri uri = Uri.parse("http://makigoto.tv/");
				intent = new Intent(Intent.ACTION_VIEW,uri);
				startActivity(intent);
				break;
		}
	}
	
	// GridViewにアダプタをセットする。
	private void setGridAdapter() {
		GridView obj_gridview1 = (GridView)findViewById(R.id.gridView1);
		obj_gridview1.setAdapter(new ImageAdapter(this));

		obj_gridview1.setOnItemClickListener(new OnItemClickListener() {
	        public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
	        	Intent intent = new Intent(getApplicationContext(), ExpandImageViewActivity.class);
	        	intent.putExtra("id", String.valueOf(position));

				startActivityForResult(intent, RESULTCODE);

	        }
	    });
	}

	public class ImageAdapter extends BaseAdapter {
	    @SuppressWarnings("unused")
		private Context mContext;

	    public ImageAdapter(Context c) {
	        mContext = c;
	    }

	    public int getCount() {
	        return imageData.size();
	    }

	    public Object getItem(int position) {
	        return null;
	    }

	    public long getItemId(int position) {
	        return 0;
	    }

	    // Adapterから参照される新しいImageViewを作成
	    public View getView(int position, View convertView, ViewGroup parent) {
	    	HashMap<String, String> mp = imageData.get(position);

	    	// インスタンスが生成されていない場合、作成
	        if (convertView == null) { 
	            convertView = getLayoutInflater().inflate(R.layout.image_row, parent, false);
	        }
	        AQuery mAq = new AQuery(convertView);
	        mAq.id(R.id.imagelist_thum).image(Const.apiDomain+mp.get("thum"));
	        mAq.id(R.id.imagelist_title).text(mp.get("title"));

	        return convertView;
	    }
	}

	/**
	 * 他のActivityから戻ってきたときの処理
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == RESULTCODE && resultCode == RESULT_OK){
			//Toast.makeText(this, "画面を閉じました", Toast.LENGTH_SHORT).show();
		}
	}

}
