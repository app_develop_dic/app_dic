package jp.gr.java_conf.art_gomaki;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class ImageDetailActivity extends Activity implements OnClickListener {

	private Button btn1, btn2;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		setContentView(R.layout.image_view);
	}

	public void onClick(View v) {
		if(v == btn1) {
			Intent intent = new Intent(this, MainActivity.class);
			startActivity(intent);
		}
		if(v == btn2) {
			Intent intent = new Intent(this, ImageListActivity.class);
			startActivity(intent);
		}
	}
}