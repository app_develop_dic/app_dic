package jp.gr.java_conf.art_gomaki;

public class FriendsSearch {
	private CharSequence fsName;
	private int fsAppInstall;

	public FriendsSearch() {
		fsName = "";
		fsAppInstall = 0;
	}

	public CharSequence getName() {
		return fsName;
	}

	public void setName(CharSequence name) {
		fsName = name;
	}

	public int getAppInstall() {
		return fsAppInstall;
	}

	public void setAppInstall(int appInstall) {
		fsAppInstall = appInstall;
	}

}