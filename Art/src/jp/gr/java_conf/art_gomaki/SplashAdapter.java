package jp.gr.java_conf.art_gomaki;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class SplashAdapter extends FragmentStatePagerAdapter {

  public SplashAdapter(FragmentManager fm) {
    super(fm);
  }

  @Override
  public Fragment getItem(int i) {

    switch(i){
    case 0:
      return new Splash1();
    case 1:
      return new Splash2();
    default:
      return new Splash3();
    }

  }

  @Override
  public int getCount() {
    return 3;
  }

  @Override
  public CharSequence getPageTitle(int position) {
    return "Page " + position;
  }

}