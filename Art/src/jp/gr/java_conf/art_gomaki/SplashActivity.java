package jp.gr.java_conf.art_gomaki;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class SplashActivity extends FragmentActivity {
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
 
        ViewPager mViewPager = (ViewPager)findViewById(R.id.viewpager);
        PagerAdapter mPagerAdapter = new MyPagerAdapter();
        mViewPager.setAdapter(mPagerAdapter);
    }
 
    private class MyPagerAdapter extends PagerAdapter implements OnClickListener {
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            // レイアウトファイル名を配列で指定します。
            int[] pages = {R.layout.splash1, R.layout.splash2, R.layout.splash3};
 
            LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
 
            View layout ;
            layout = inflater.inflate(pages[position], null);
            ((ViewPager) container).addView(layout);
            
            // 最終ページのボタン処理追加
            if (position == 2) {
            	Button finBtn = (Button) layout.findViewById(R.id.splash_fin_btn);
            	finBtn.setOnClickListener(this);
            }
			
            return layout;
        }
        
    	public void onClick(View v) {
    		if(v.getId() == R.id.splash_fin_btn) {
    			Intent intent = new Intent(SplashActivity.this, MainActivity.class);
    			startActivity(intent);
    		}
    	}
 
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager)container).removeView((View)object);
        }
 
        @Override
        public int getCount() {
            // ページ数を返します。
            return 3;
        }
 
        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }
    }
}