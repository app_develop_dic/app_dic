package jp.gr.java_conf.art_gomaki;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

public class FriendsSearchActivity extends Activity implements OnClickListener {
	
	private ArrayList phone_lists;
	private PhoneBookAdapter pbAdapter;
	private Button btn1, btn2, btn3, btn4;
	/**
	 * DB情報
	 */
	private String DB_NAME = "gomaki_news.db";	// DB名
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_friends_search);
		
		phone_lists = new ArrayList();
		pbAdapter = new PhoneBookAdapter(this, phone_lists);
		String DB_Path = "data/data/" + getPackageName() + "/" + DB_NAME;
		
		ListView _listview = (ListView) findViewById(R.id.listView2);
		
		// スレッドを起動する
		GetPhoneBook thread = new GetPhoneBook(this, pbAdapter, _listview);
		thread.execute(DB_Path);

		btn1 = (Button)findViewById(R.id.button1);
        btn1.setOnClickListener(this);
//		btn2 = (Button)findViewById(R.id.button2);
//        btn2.setOnClickListener(this);
		btn3 = (Button)findViewById(R.id.button3);
        btn3.setOnClickListener(this);
		btn4 = (Button)findViewById(R.id.button4);
        btn4.setOnClickListener(this);

	}

	public void onClick(View v) {
		if(v == btn1) {
			Intent intent = new Intent(this, MainActivity.class);
			startActivity(intent);
		}
		if(v == btn2) {
			Intent intent = new Intent(this, ImageListActivity.class);
			startActivity(intent);
		}
		else if(v == btn3) {
			Intent intent = new Intent(this, FriendsSearchActivity.class);
			startActivity(intent);
		}
		else if(v == btn4) {		
			Uri uri = Uri.parse("http://makigoto.tv/");
			Intent intent = new Intent(Intent.ACTION_VIEW,uri);
			startActivity(intent);
		}
	}
	
}
