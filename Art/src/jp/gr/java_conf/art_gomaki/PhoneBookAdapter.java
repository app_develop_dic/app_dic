package jp.gr.java_conf.art_gomaki;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class PhoneBookAdapter extends ArrayAdapter<FriendsSearch> {

	private LayoutInflater fsInflater;
	private TextView fsLeft;
	private TextView fsRight;

	public PhoneBookAdapter(Context context, List<FriendsSearch> objects) {
		super(context, 0, objects);
		// TODO Auto-generated constructor stub
		fsInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	// 一行ごとのビューを作成する。
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (convertView == null) {
			view = fsInflater.inflate(R.layout.friends_search_row, null);
		}

		FriendsSearch item = this.getItem(position);
		if (item != null) {
			if(item.getAppInstall() == 1) {
				String name = item.getName().toString();
				if(!name.equals("")) {
					fsLeft = (TextView) view.findViewById(R.id.fs_left);
					fsLeft.setText(name);
					fsRight = (TextView) view.findViewById(R.id.fs_right);
					fsRight.setText("アプリ使用済");
				}
			}
		}
		return view;
	}
}