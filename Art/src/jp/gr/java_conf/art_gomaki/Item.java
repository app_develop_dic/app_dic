package jp.gr.java_conf.art_gomaki;

public class Item {
	private CharSequence mTitle;
	private CharSequence mDescription;
	private CharSequence mDate;
	private CharSequence mLink;

	public Item() {
		mTitle = "";
		mDescription = "";
		mDate = "";
		mLink = "";
	}

	public CharSequence getDescription() {
		return mDescription;
	}

	public void setDescription(CharSequence description) {
		mDescription = description;
	}

	public CharSequence getTitle() {
		return mTitle;
	}

	public void setTitle(CharSequence title) {
		mTitle = title;
	}

	public CharSequence getPDate() {
		return mDate;
	}

	public void setPDate(CharSequence pubDate) {
		mDate = pubDate;
	}

	public CharSequence getLink() {
		return mLink;
	}

	public void setLink(CharSequence link) {
		mLink = link;
	}

}